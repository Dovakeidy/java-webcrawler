import javax.swing.*;

/**
 * Classe main
 * @author Aurélien RAIMBAULT-CHARLET
 * @version 1.0
 */
public class WebCrawler {
    private static Core core = Core.getInstance();

    /**
     * Starting of the web crawler.
     * @param args  Starting URL and needle to search.
     */
    public static void main(String[] args) {
        try {
            String startingUrl = "";
            String needle = "";

            if(args.length != 2) {
                startingUrl = JOptionPane.showInputDialog("Enter a starting URL to crawl");
                needle = JOptionPane.showInputDialog("Enter a needle to search");
            } else {
                startingUrl = args[0];
                needle = args[1];
            }

            core.setParameters(startingUrl, needle);

            core.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
