import java.net.*;
import java.io.*;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Read an URL and find the needle in the website refered to
 * @author Aurélien RAIMBAULT-CHARLET
 * @version 1.0
 */
public class URLReader implements Runnable {
    private Pattern pattern = Pattern.compile("href=\"([^\"]*)\"", Pattern.DOTALL);
    private Pattern needle;
    private String url;
    private int needleCounter = 0;
    private boolean foundNeedle = false;
    private LinkedList<String> urls = new LinkedList<>();
    private Core core = Core.getInstance();

    /**
     * Set the URL and needle.
     * @param _url   The URL to visit.
     * @param needle   The needle to search.
     */
    public URLReader(String _url, String needle) {
        this.url = _url;
        this.needle = Pattern.compile(".*"+ needle +".*");
    }

    /**
     * Search the needle in the URL given,
     * display informations about it and add URLs that contain the needle to the web crawler queue.
     */
    @Override
    public void run() {
        core.incThreadsCounter();

        BufferedReader bufferedReader = null;
        String inputLine = null;

        try {
            URL currentUrl = new URL(url);
            // Open a stream at the given URL
            bufferedReader = new BufferedReader(new InputStreamReader(currentUrl.openStream()));

            while (true) {
                // If we finished the reading of the URL
                if (!((inputLine = bufferedReader.readLine()) != null)) {
                    // If the needle has been found at least 1 time
                    if(foundNeedle) {
                        System.err.print(Thread.currentThread().getName());
                        System.out.println(" URL : " + url + " FOUND : " + this.needleCounter);
                        // Set all the URLs founded in the executor service "queue"
                        for(String localUrl : this.urls) {
                            this.core.addUrl(localUrl);
                        }
                    }

                    break;
                } else {
                    Matcher hrefMatcher = this.pattern.matcher(inputLine);
                    Matcher needleMatcher = this.needle.matcher(inputLine);

                    // If the needle is found, set foundNeedle to true
                    if(needleMatcher.find()) {
                        this.foundNeedle = true;
                        this.needleCounter++;
                    }

                    // If a href is found, add the url in an array
                    if (hrefMatcher.find()) {
                        this.urls.add(hrefMatcher.group(1));
                    }
                }
            }

        } catch (Exception e) { }

        try {
            bufferedReader.close();
        } catch (Exception e) { }

        core.decThreadsCounter();

    }
}