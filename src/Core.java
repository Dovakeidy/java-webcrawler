import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Singleton class that manage the Threads
 * @author Aurélien RAIMBAULT-CHARLET
 * @version 1.0
 */
public class Core {
    private static ExecutorService executorService;
    private static int availableProcessors;

    private static String needle;
    private static String startingUrl;
    private static Core single_instance = null;
    private static AtomicInteger threadsCounter = new AtomicInteger();

    private static ConcurrentLinkedQueue<String> visitedUrls = new ConcurrentLinkedQueue<>();

    /**
     *  Return or create an instance if non existing instance.
     * @return single_instance  Return the singleton instance
     */
    public static Core getInstance() {
        if (single_instance == null)
            single_instance = new Core();

        return single_instance;
    }

    /**
     * Set the fixedThreadPool at availableProcessors number.
     */
    private Core() {
        availableProcessors = Runtime.getRuntime().availableProcessors();
        executorService = Executors.newFixedThreadPool(availableProcessors);
    }

    /**
     * Increment the threads counter.
     */
    public static void incThreadsCounter() {
        threadsCounter.incrementAndGet();
    }

    /**
     * Decrement the threads counter and stop the Executor Service if there is not threads left.
     */
    public static void decThreadsCounter() {
        if(threadsCounter.decrementAndGet() == 0) {
            Core.shutdown();
        }
    }

    /**
     * Setting parameters ( starting URL and needle ).
     * @param _startingUrl   The starting URL.
     * @param _needle   The needle to search.
     */
    public static void setParameters(String _startingUrl, String _needle) {
	    System.err.println("URL : " + _startingUrl);
        System.err.println("NEEDLE : " + _needle);
        System.err.println("PROCESSORS USED : " + availableProcessors);

        startingUrl = _startingUrl;
        needle = _needle;
    }

    /**
     * Add URL in a thread and submit to the executorService queue.
     * @param url   The URL to add.
     */
    public static void addUrl(String url) {
        if (!visitedUrls.contains(url)) {
            visitedUrls.add(url);

            executorService.submit(new URLReader(url, needle));
        }
    }

    /**
     * Start the program with the starting URL given.
     */
    public static void start() {
        System.err.println("Program started..");
        addUrl(startingUrl);
    }

    /**
     * Shutdown the program.
     */
    public static void shutdown() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                System.err.println("Program finished..");

                if (!executorService.awaitTermination(10, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");
            }
        } catch (InterruptedException ie) {
            executorService.shutdownNow();

            Thread.currentThread().interrupt();
        }

    }

}
