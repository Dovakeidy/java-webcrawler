# WebCrawler

A simple Java web crawler.

Start the program with a URL and a needle and let the program dive into the web to find occurency of it ! 

# Required
chmod 700 start.sh

# Start with initial parameters
./start.sh [arg1] [arg2] (ex: ./start.sh https://fr.wikipedia.org/wiki/Nantes Nantes)

# Start with user input
./start.sh