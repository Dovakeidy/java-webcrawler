#!/bin/bash
# This script will start the web crawler of Aurélien RAIMBAULT-CHARLET

# Create out folder to place .class file
echo "Creating out folder"
mkdir out

# Compile source file to out folder
echo "Compiling.."
javac src/*.java -d out

# Make a jar out of class file
echo "Making jar"
jar cfe WebCrawler.jar WebCrawler -C out .

# Generating javadoc
echo "Generating javadoc"
javadoc src/*.java -d javadoc

# Start the web crawler program
echo "Starting program"
echo " "
if [ "$#" = "2" ];
    then
        java -jar WebCrawler.jar $1 $2
    else
        java -jar WebCrawler.jar
fi

exit